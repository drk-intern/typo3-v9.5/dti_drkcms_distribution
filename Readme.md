## Distribution für die Installation bei DTInternet

### Erweiterungen

#### DRK Erweiterungen

* drk/drk-distribution2016

#### Öffentliche Erweiterungen

* beechit/fal-securedownloa
* lochmueller/staticfilecache
* schommermedia/smedia-course
* t3monitor/t3monitoring_clie

#### Anpassungen von DTInternet

* dtinternet/dti-brkblooddonation
* dtinternet/dti-chatbot
* dtinternet/dti-cookienote
* dtinternet/dti-drk-custom
* dtinternet/dti-drkdesigns
* dtinternet/dti-hiorg
* dtinternet/dti-menueservice
* dtinternet/dti-t3branding

Für einige der Erweiterungen sind Zugangsdaten zum Gitlab der
DTInternet GmbH notwendig.

Für berechtigte Personen finden sich die Zugangsdaten hier:

* https://projekte.dt-internet.de/projects/77?modal=Task-4460-77

Für andere Personen muss eine entsprechende Anfrage bei DTInternet gestellt
werden. Ein Recht auf öffentlichen Zugang besteht nicht.
