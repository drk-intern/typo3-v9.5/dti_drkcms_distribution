<?php
/*
 * Copyright (c) 2022 Stefan Bublies@dt-internet.de
 *
 * @link     https://www.dt-internet.de
 * @package  DRK
 *
 */
/***************************************************************
 * Extension Manager/Repository config file for ext: 'dti_drkcms_distribution'
 *
 * Manual updates:
 * Only the data in the array - anything else is removed by next write.
 * 'version' and 'dependencies' must not be touched!
 ***************************************************************/

$EM_CONF['dti_drkcms_distribution'] = array(
    'title' => 'D&T Internet DRKCMS-Distribution',
    'description' => 'D&T Internet DRKCMS Distribution',
    'category' => 'templates',
    'author' => 'Stefan Bublies',
    'author_email' => 'bublies@dt-internet.de',
    'state' => 'stable',
    'version' => '12.10.0',
    'constraints' => array(
        'depends' => array(
            'typo3' => '11.5.0-11.5.99',
        ),
        'conflicts' => array(),
    ),
);
